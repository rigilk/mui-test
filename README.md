# Design challenge

### Why
We would like to get a sample of your work on a problem specific to Material-UI. While asking questions on an existing portfolio can be very helpful to understand your problem-solving approach. We think that seeing them used in one of our projects would significantly help us get a glimpse of the potential of a future collaboration (for both of us hopefully).
The problem

This design challenge focuses on the Material-UI's products.

In the header of the branding, we link "Docs", "Material-UI X", "Pricing", "Templates", "About Us". While this approach is good enough for the current state of the company, we will need to create more pages to present the different products we are investing in resources to grow. So far, we have, more or less, identified the following problems that can have a dedicated solution/product.

* @material-ui/system as an improved version of TailwindCSS.
* @material-ui/unstyled as an improved version of [https://reach.tech/].
* Material-UI X as an improved version of Kendo-UI/ag-Grid
* A store of templates as an improved version of [https://themeforest.net/].
* Design kits for Sketch/Figma/AdobeXD/Framer.
* The core components. They are well known for implementing Material Design and being customizable. We are investing in building a second theme to complement Material Design, e.g Ant design or TailwindUI.
* Material UI Builder, a low-code effort as an improved version of retool.com.

We need a design to clearly communicate the main value proposition of the company, clearly organize the different products, and be compelling (looks great) for the audience.

### The constraints
Your mission in this challenge is to:

1. To think about the Material-UI products holistically. What is the key value proposition in each of them and how they build together a broader vision for the company?
2. Designing the hero section of the homepage and each of the products (at least, the most important ones). With wordings expressing the main value proposition.
3. Design the nav header to scale to the presentation of these different products.

The higher the fidelity of the design is, the better. The challenge is meant to be time-boxed to 4 hours. If you significantly diverge from this target, it’s fine, please let us know the actual time spent.
If the time constraint is too short, please prioritize covering fewer pages instead of sacrificing the quality of the execution.

We don’t ask to implement the design pages in HTML/CSS. It’s unlikely that the time constraint allows it.

